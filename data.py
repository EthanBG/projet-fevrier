# coding: utf-8

import csv

#Liste des phrases du présentateur en cas de bonne réponse
phrasesG = ["Bien joué, mais, honnêtement mon petit cousin de 4 ans aurait eu bon aussi alors... ", "Je doit admettre que je ne l'avais pas celle la... ", "Très bonne réponse !!! ", "Comme le dirais mon amis Julien : Ah oui oui ouiii ! ", "Très bien joué ! ", "Réponse facile n'est-ce pas ? "]

#Liste des phrases du présentateur en cas de mauvaise réponse
phrasesP = ["Dommage, c'est perdu ! ", "Bon bah vous ferez mieux la prochaine fois, car c'est la mauvaise réponse. ", "Mauvaise réponse ! ", "Malheureusement, c'est perdu ! ", "Bon ben, vous reviendrez une prochaine fois ", "On espère vous revoir une prochaine fois ! "]

#Liste des phrases d'interlude du présentateur
interlude = ["Bon aller on passe à la suite, j'ai pas que ça à faire moi. ", "Allons y pour la question suivante... ", "Continuons. ", "Bravo, on se rapproche du million ! ", "Et si on passait à la suite ? ", "Allez, continuons ! "]

#Liste des paliers d'argents correspondant au numéros de la question.
paliers = {0: 100, 1: 200, 2: 300, 3: 500, 4: 1000, 5: 2000, 6: 4000, 7: 8000, 8: 12000, 9: 24000, 10: 36000, 11: 72000, 12: 150000, 13: 300000, 14: 1000000}

#Liste des questions pour continuer
questContinuer = ["Bon, c'est l'heure de la question fatidique : Souhaitez vous continuer pour "]

#Définition de la fonction permetant de convertir un .csv en liste de dictionnaires (celle vue en cours)
def depuis_csv(nom_fichier_csv):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r'))
    return [dict(ligne) for ligne in lecteur]

#quest sera la liste de des dictionnaires : {"quest": "LA QUESTION", "repG": "LA BONNE REPONSE", "repP" : "PROPOSITION 1;PROPOSITION 2;PROPOSITION 3;PROPOSITION 4"}
quest = depuis_csv("questions.csv")

#Avec cette boucle, on transforme la chaine de caractère de repP en tuple : {"quest": "LA QUESTION", "repG": "LA BONNE REPONSE", "repP" : ("PROPOSITION 1", "PROPOSITION 2", "PROPOSITION 3", "PROPOSITION 4")}
for qestion in quest:
    qestion["repP"] = tuple(map(str, qestion["repP"].split(';')))
