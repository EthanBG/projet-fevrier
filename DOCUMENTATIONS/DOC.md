# QUI VEUT GAGNER DES MILLIONS ?
## PREREQUIS :
1- Un terminal

------
2- Python 3 ou supérieur  et pip:
#### LINUX :
__DEBIAN__:

    $ sudo apt-get install python3 python3-pip

__FEDORA__:

    $ sudo dnf install python3
#### WINDOWS :

Suivre [cette documentation](https://www.python.org/downloads/release/python-381/).
Pour pip, télécharger [ce fichier](https://bootstrap.pypa.io/get-pip.py) puis dans un terminal, exécuter le fichier avec :

    $ python get-pip.py

#### MACOS:

    $ brew install python
-----
3- Les paquets python suivants :

`easygui` :

    $ pip install easygui

Il est possible que lors de l'exécution du programme quiz.py, un message d'erreur impliquant Tkinter apparaisse. Dans ce cas :

    $ sudo dnf install python3-tkinter

`random` :

    $ pip install random

`playsound` :

    $ pip install playsound

`csv` :

    $ pip install csv
----
4- Télécharger le dépôt GitLab :

Premièrement, rendez vous sur [le dépôt GitLab](https://gitlab.com/EthanBG/projet-fevrier) puis téléchargez le au format .zip à l'aide du bouton Download à coté du bouton Clone, bleu, en haut à droite de la page, puis sélectionnez .zip .

---

#### VOUS POUVEZ MAINTENANT EXECUTER LE PROGRAMME ET VOUS LAISSEZ GUIDER PAR LES INSTRUCTIONS :

    python quiz.py
