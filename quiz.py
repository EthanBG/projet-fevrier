# coding: utf-8

#Importation des bibliotheques nécessaires
import easygui as eg
import random as ran
from playsound import playsound

#Importation de mes données se trouvant dans "data.py"
import data as d


def continuer(numéroQuestion: int):
    """
    Permet de demander à l'utilisateur s'il souhaite continuer ou repartir avec ses gains
    """
    title = "CONTINUER ?"
    msg = ran.choice(d.questContinuer) + f"{d.paliers[numéroQuestion+1]}€, ou vous arêter ici avec {d.paliers[numéroQuestion]}€"
    choices = ("C'EST PARTI", "JE PREFERE M'ARETER LA.")
    if not eg.ynbox(msg, title, choices):
        playsound("sounds/win.mp3")
        eg.msgbox(f"Très bien, merci d'avoir jouer ! Vous repartez avec {d.paliers[numéroQuestion]}€.")
        exit()
    else:
        playsound("sounds/selection.mp3")

def verifReponse(réponse: str, numéroQuestion: int):
    """
    Vérifie que la réponse de l'utilisateur est correct
    """
    playsound("sounds/choice.mp3")
    #Si la réponse est bonne on choisi au hasard une phrase de bonne réponse puis une interlude.
    if réponse == d.quest[numéroQuestion]["repG"]:
        playsound("sounds/correct.mp3")
        msg = ran.choice(d.phrasesG)
        title = "BIEN JOUE"
        msg = msg + ran.choice(d.interlude)
        btn = "CONTNUER"
        eg.msgbox(msg, title, btn)
        playsound("sounds/selection.mp3")
        #Si la question actuel n'est pas la dernière, on demande à l'utilisateur s'il souhaite continuer, sinon il a gagné
        if numéroQuestion != 14:
            continuer(numéroQuestion)
    #Si la réponse est mauvaise on choisi au hasard une p
    else:
        playsound("sounds/wrong.mp3")
        title = "PERDU"
        msg = ran.choice(d.phrasesP) + "La bonne réponse étant " + d.quest[numéroQuestion]["repG"] + " Vous perdez donc la totalité de vos gains :( Merci d'avoir joué cependant !"
        btn = "ADIEU"
        eg.msgbox(msg, title, btn)
        playsound("sounds/fail.mp3")
        exit()
    return None

def poserQuestion(numéroQuestion: int):
    """
    Permet de poser une question
    """
    title = "QUESTION N°" + str(numéroQuestion)
    msg = d.quest[numéroQuestion]["quest"]
    choices = d.quest[numéroQuestion]["repP"]
    verifReponse(eg.buttonbox(msg, title, choices), numéroQuestion)
    return None

def start():
    for i in range(0, len(d.quest)):
        #Pour toutes les questions, on les pose.
        poserQuestion(i)
    #Si on sort de la boucle, alors l'utilisateur repart avec le "million" donc on célèbre ça
    title = "GAGNEEEEEEEEE !!!!"
    msg = "Vous gagnez le millionnnnnnn ! Félicitation ! "
    btn = "MERCI !"
    eg.msgbox(msg, title, btn)
    playsound("sounds/champion.mp3")
    return None


#Présentation et lancement du jeu
title = "BIENVENUE"
msg = "Bienvenue dans ce quiz de culture général"
btn = "Je suis où là ?"
eg.msgbox(msg, title, btn)
playsound("sounds/selection.mp3")

title = "EXPLICATION"
msg = "Vous êtes dans le grand quiz de culture général, à la clé : le prestige... Je suis Marcel et vous ?"
btn = "Confirmer"
prenom = eg.enterbox(msg, title)
playsound("sounds/selection.mp3")

title = "TUTORIEL"
msg = f"Très bien {prenom}, enchanté de vous connaître. Pour vous apprendre à jouer, nous allons procéder à un petit tutoriel : Choisissez parmis les quatre propositions, la réponse qui vous semble correcte. Fin du tutoriel."
btn = "Allons-y !"
eg.msgbox(msg, title, btn)
playsound("sounds/selection.mp3")

start()
